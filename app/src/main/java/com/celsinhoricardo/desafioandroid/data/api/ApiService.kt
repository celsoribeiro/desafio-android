package com.celsinhoricardo.desafioandroid.data.api

import com.celsinhoricardo.desafioandroid.data.model.Repository
import io.reactivex.Single

interface ApiService {

    fun getRepositories(page : String): Single<Repository>

}