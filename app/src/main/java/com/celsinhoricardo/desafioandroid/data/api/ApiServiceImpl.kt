package com.celsinhoricardo.desafioandroid.data.api

import com.celsinhoricardo.desafioandroid.data.model.Repository
import com.rx2androidnetworking.Rx2AndroidNetworking
import io.reactivex.Single

class ApiServiceImpl : ApiService {

    override fun getRepositories(page : String): Single<Repository> {
        return Rx2AndroidNetworking.get("https://api.github.com/search/repositories?q=language:kotlin&sort=stars&page=$page")
            .build()
            .getObjectSingle(Repository::class.java)
    }

}