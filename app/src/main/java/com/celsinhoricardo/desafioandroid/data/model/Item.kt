package com.celsinhoricardo.desafioandroid.data.model

import com.google.gson.annotations.SerializedName

data class Item(
    val id: Long,

    @SerializedName("node_id")
    val nodeID: String,

    val name: String,

    @SerializedName("full_name")
    val fullName: String,

    val private: Boolean,
    val owner: Owner,

    @SerializedName("stargazers_count")
    val stargazersCount: Long,


    @SerializedName("forks_count")
    val forksCount: Long,

    val score: Double
)
