package com.celsinhoricardo.desafioandroid.data.model

import com.google.gson.annotations.SerializedName

data class Repository(
    @SerializedName("total_count")
    val totalCount: Long,

    val items: List<Item>
)
