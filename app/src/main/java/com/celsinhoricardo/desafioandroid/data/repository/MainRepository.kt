package com.celsinhoricardo.desafioandroid.data.repository

import com.celsinhoricardo.desafioandroid.data.api.ApiService
import com.celsinhoricardo.desafioandroid.data.model.Repository
import io.reactivex.Single

class MainRepository(private val apiService: ApiService) {

    fun getRepositories(page : String): Single<Repository> {
        return apiService.getRepositories(page)
    }

}