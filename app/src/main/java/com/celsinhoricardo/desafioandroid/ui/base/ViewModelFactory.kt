package com.celsinhoricardo.desafioandroid.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.celsinhoricardo.desafioandroid.data.api.ApiService
import com.celsinhoricardo.desafioandroid.data.repository.MainRepository
import com.celsinhoricardo.desafioandroid.ui.main.viewmodel.MainViewModel

class ViewModelFactory(private val apiService: ApiService) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(MainRepository(apiService)) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}