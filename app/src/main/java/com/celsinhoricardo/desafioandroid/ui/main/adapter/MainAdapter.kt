package com.celsinhoricardo.desafioandroid.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.celsinhoricardo.desafioandroid.data.model.Item
import com.celsinhoricardo.desafioandroid.databinding.ItemLayoutBinding


class MainAdapter(
    private val items: ArrayList<Item?>
) : RecyclerView.Adapter<MainAdapter.DataViewHolder>() {

    class DataViewHolder(val binding: ItemLayoutBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Item?) {
            binding.tvOwnerName.text = item?.owner?.login
            binding.tvRepoName.text = item?.name
            binding.tvStarsCount.text = "Star ${item?.stargazersCount}"
            binding.tvForksCount.text = "Fork ${item?.forksCount}"

            Glide.with(binding.ivAvatar.context)
                .load(item?.owner?.avatarURL)
                .into(binding.ivAvatar)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        val binding = ItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DataViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
            holder.bind(items[position])
    }

    fun addData(list: List<Item>) {
        items.addAll(list)
        notifyDataSetChanged()
    }

}