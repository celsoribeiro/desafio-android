package com.celsinhoricardo.desafioandroid.ui.main.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.celsinhoricardo.desafioandroid.data.api.ApiServiceImpl
import com.celsinhoricardo.desafioandroid.data.model.Item
import com.celsinhoricardo.desafioandroid.databinding.ActivityMainBinding
import com.celsinhoricardo.desafioandroid.ui.base.ViewModelFactory
import com.celsinhoricardo.desafioandroid.ui.main.adapter.MainAdapter
import com.celsinhoricardo.desafioandroid.ui.main.viewmodel.MainViewModel
import com.celsinhoricardo.desafioandroid.utils.OnLoadMoreListener
import com.celsinhoricardo.desafioandroid.utils.RecyclerViewLoadMoreScroll
import com.celsinhoricardo.desafioandroid.utils.Status

class MainActivity : AppCompatActivity() {

    private lateinit var mainViewModel: MainViewModel
    private lateinit var adapter: MainAdapter
    private lateinit var binding: ActivityMainBinding
    private var currentPage = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupUI()
        setupViewModel()
        setupObserver()
        mainViewModel.fetchRepositories(currentPage.toString())
    }

    private fun setupUI() {
        val mLayoutManager = LinearLayoutManager(this)
        val scrollListener = RecyclerViewLoadMoreScroll(mLayoutManager)

        binding.recyclerView.layoutManager = mLayoutManager
        adapter = MainAdapter(arrayListOf())
        binding.recyclerView.addItemDecoration(
            DividerItemDecoration(
                binding.recyclerView.context,
                (binding.recyclerView.layoutManager as LinearLayoutManager).orientation
            )
        )

        scrollListener.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                loadMoreData()

            }
        })
        binding.recyclerView.addOnScrollListener(scrollListener)
        binding.recyclerView.adapter = adapter
    }

    private fun setupObserver() {
        mainViewModel.getRepositories().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    binding.recyclerView.visibility = View.VISIBLE
                    it.data?.let { repository -> renderList(repository.items) }
                }
                Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun renderList(users: List<Item>) {
        adapter.addData(users)
        adapter.notifyDataSetChanged()
    }

    private fun setupViewModel() {
        mainViewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(ApiServiceImpl())
        ).get(MainViewModel::class.java)
    }

    private fun loadMoreData() {
        Handler(Looper.getMainLooper()).postDelayed({
            currentPage = currentPage + 1
            mainViewModel.fetchRepositories(currentPage.toString())
        }, 1000)

    }
}