package com.celsinhoricardo.desafioandroid.ui.main.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.celsinhoricardo.desafioandroid.data.model.Repository
import com.celsinhoricardo.desafioandroid.data.repository.MainRepository
import com.celsinhoricardo.desafioandroid.utils.Resource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainViewModel(private val mainRepository: MainRepository) : ViewModel() {

    val repositories = MutableLiveData<Resource<Repository>>()
    val compositeDisposable = CompositeDisposable()



     fun fetchRepositories(page : String) {
        repositories.postValue(Resource.loading(null))
        compositeDisposable.add(
            mainRepository.getRepositories(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ repoList ->
                    repositories.postValue(Resource.success(repoList))
                }, { throwable ->
                    repositories.postValue(Resource.error("Sistema indisponível", null))
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun getRepositories(): LiveData<Resource<Repository>> {
        return repositories
    }

}