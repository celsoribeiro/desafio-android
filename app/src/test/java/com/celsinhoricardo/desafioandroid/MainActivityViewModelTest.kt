package com.celsinhoricardo.desafioandroid

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.celsinhoricardo.desafioandroid.data.api.ApiServiceImpl
import com.celsinhoricardo.desafioandroid.data.model.Item
import com.celsinhoricardo.desafioandroid.data.model.Repository
import com.celsinhoricardo.desafioandroid.data.repository.MainRepository
import com.celsinhoricardo.desafioandroid.ui.main.viewmodel.MainViewModel
import com.celsinhoricardo.desafioandroid.utils.Resource
import io.reactivex.Maybe
import io.reactivex.Single
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class MainActivityViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    lateinit var mainRepository: MainRepository
    lateinit var mainViewModel: MainViewModel


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mainRepository = mock(MainRepository::class.java)
        mainViewModel = MainViewModel(mainRepository)
    }

    @Test
    fun `fetch repositories then return not null`() {

        val response: Single<Repository> = Single.just(Repository(12,ArrayList()))


        Mockito.`when`(mainRepository.getRepositories("1")).thenReturn(response)
        val observer = mock(Observer::class.java) as Observer<Resource<Repository>>

        mainViewModel.getRepositories().observeForever(observer)
        mainViewModel.fetchRepositories("1")
        assertNotNull(mainViewModel.getRepositories())


    }

}